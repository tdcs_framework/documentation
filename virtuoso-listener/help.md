The operations can also be accessed at your listener addresss `/admin/help` i.e. `localhost:8080/virtuoso-listener/admin/help`

# v2.0.1

## Admin Operations
**Note:** All operations to be preceded by the admin servlet path as set in web.xml. 
Default `/admin`

| **Operation** | **Parameters** | **Description** |
| ------------- | -------------- | --------------- |
|**GET /configuration**| |Display all registered message types and all registered response details.|
|**GET /list**| |Display the IDs of all stored messages.|
|**GET|POST /query**| |Query a specific item from the message store.<br>The request must have a serialised MessageKey in the payload|
|**GET /search**| |Search the message store with a given criteria.<br>The request must have a serialised `Matcher<HttpServletRequest>` in the payload|
|**POST /de-register-response**| |De-register a response so that it's no longer used by the stub service.<br>The request must have a serialised `Matcher<HttpServletRequest>` in the payload|
|**POST /de-register-type**| |De-register a message type so that it's no longer handled by the stub service.<br>The request must have a serialised `Matcher<HttpServletRequest>` in the payload|
|**POST /purge**| |Purge all registered message types, registered responses, and store messages.|
||& responses={`TRUE`&#124;`FALSE`}|**Optional:** Controls whether to purge registered responses.<br>**Default:** `TRUE`.|
||& store={`TRUE`&#124;`FALSE`}|**Optional:** Controls whether to purge stored messages.<br>**Default:** `TRUE`.|
||& type={`TRUE`&#124;`FALSE`}|**Optional:** Controls whether to purge registered message types.<br>**Default:** `TRUE`.|
|**POST /register-response**| |Register details of a response to be sent back when receiving messages of a given response.The request must have a serialised `ResponseType` in the payload|
|**POST /register-type**| |Register a new message type to be handled by the stub service.<br>The request must have a serialised `MessageType` in the payload|
||& override={`TRUE`&#124;`FALSE`}|**Optional:** Allow re-registration or not.<br>**Default:**`FALSE`.|
|**POST /remove**| |Remove a specific item from the store of received messages.<br>The request must have a serialised `MessageKey` in the payload|
|**GET&#124;POST /help**| |Display this help message.|


## Stub Services
**Note:** All operations to be preceded by the stub servlet path as set in web.xml.  
Default `/service`

| **Operation** | **Identifier** | **Description** |
| ------------- | -------------- | --------------- |
| **DELETE &#124; GET &#124; HEAD &#124; OPTIONS &#124; POST &#124; PUT &#124; TRACE /&#42;**| | Listens for incoming messages, handling only those matching a registered type.  See `/admin/register-type`. A response will be sent back to the originator.  See `/admin/register-response`.|


