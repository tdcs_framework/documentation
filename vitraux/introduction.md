Vitraux is a web-server that is used to autonomously handle executions of automated or performance tests. It wraps existing test automation, load testing, and monitoring tools, in order to orchestrate a test execution, while gathering and reporting its results. Interfaces to different tools and technologies can be developed to extend Vitraux's capabilities and better target the specific needs of each project. 
Vitraux is deployed on a Linux server, which will be referred to as a Vitraux server. Vitraux runs as a servlet on Apache Tomcat. Any test automation, load testing, utility, or monitoring tool that is used by Vitraux has to be installed on the Vitraux server. Examples of such external tools are:
- Test Automation: JUnit, Selenium
- Load Testing: JMeter, Webpagetest
- Monitoring: vmstat, sar
- Utility: gnuplot, xvfb, ant, mvn
The location of the executables of these external programs can be defined as config.properties of the Vitraux servlet.

Once the Vitraux server and all external tools are correctly deployed (see Installation), the web-server will listen for new requests. These are either:
- POST requests that register a “test assembly” to this Vitraux server
- GET requests that execute a previously registered test assembly
- GET requests that poll for the execution status of a test assembly

A "test assembly” is a zip file that bundles all necessary information to execute a test. This includes test data, any special tools or scripts, and configuration files. The registration of a test assembly to a Vitraux server extracts this bundle to a specified location (i.e. VITRAUX_HOME) and links the extraction directory to a unique test name for this assembly. This correlation of the "test assembly directory" to a "unique test name" is stored on a MySQL database on the Vitraux server.

The unique test name for the assembly is then used in requests to execute a previously registered test. The Vitraux server can set up a provided environment.properties file, to allow for the same test to run against different environments. Once a test starts, the Vitraux server can be queried with GET requests to return a JSON message with the status of the test, together with the current output. This allows for live monitoring of a test's execution.

Two types of tests can be triggered by Vitraux, when a new execution request arrives: Automation Tests and Performance Tests.

## Automation (Vitraux-Virtuoso) ##

**A1.** Automation Tests rely on the Virtuoso library, which is a Java library that offers support for a set of commonly used test types with JUnit. 

**A2.** Vitraux starts a new Virtuoso execution with the current working directory being the "test assembly directory" corresponding to the requested test name. The user account that executes the Virtuoso (JVM) process is exactly the same as the one that runs the Tomcat server and thus any required permissions to successfully run this test need to be set up correctly for that user account. 

**A3.** Vitraux overrides the output directory selection of the Virtuoso config.properties file and outputs all results under a specified results directory within the "test assembly directory". At the end of the execution, this output directory is compressed into a zip file that becomes available on the Tomcat web-server under the results/ path.   

## Performance (Vitraux-Maestro) ##

**P1.** Performance Tests rely on the Maestro framework, which is a Java library that fully orchestrates a performance test execution by wrapping existing tools. It supports set-up and clean-up activities, monitoring set-up on the servers of the System under Test, triggering of the load test program, and gathering and presentation in tables and graphs of the performance test results. An XML file, called plan.xml, needs to be defined at the root of the registered test assembly zip file, to define all these different types of activities. 

**P2.** Vitraux starts a new Maestro execution with the current working directory being the "test assembly directory" corresponding to the requested test name. The user account that executes the Maestro process is exactly the same as the one that runs the Tomcat server and thus any required permissions to successfully run this test need to be set up correctly for that user account. This user account also needs to have set-up public-key authentication towards all monitored servers, so as to automatically remote ssh and gather performance metrics without a password prompt. 

**P3.** At the end of the execution, the various result files that are described by the plan.xml are compressed into a zip file that becomes available on the Tomcat web-server under the results/ path.   
