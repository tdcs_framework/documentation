## web.xml
The web xml defines servlet context parameters. The $server_type parameter below can have either take the “virtuoso” or	“maestro” values.


| **Key** | **Description** | **Acceptable values** | **Required** | **Default** |
| --- | ----------- | ----------------- | -------- | ------- |
|performanceTestSuitesRegistry|VITRAUX_HOME directory where all tests are extracted and from where they are executed|Unix directory name|Mandatory|/var/data/$server_type|
|performanceTestSuitesDatabaseName|The database name where information about the tests are held|Valid MySQL database name|Mandatory|vitraux|
|testAutomationResults|String to identify result files in JUnit results	String prefix|Only Vitraux-Virtuoso|'Result files: '|
|seleniumDisplay|The DISPLAY where UI tests can be rendered|Typically the DISPLAY where Xvfb is running|Only Vitraux-Virtuoso|:99|
|Multithreaded|Determine whether to allow for multiple tests to be served concurrently (Virtuoso) or to only execute a single test at a time (Maestro)|True or false|Optional|false|
|Performance Test Database|The JDBC resource name of the Vitraux database|Valid database resource name|Mandatory|jdbc/vitraux|
|build_executable_mvn|The Maven 3.0 executable to run tests|Executable name or filepath|Only for Virtuoso|mvn|


## config.properties
The config.properties file defines result output information and third party tool locations and parameters.

|**Key**|**Description**|**Acceptable values**|**Required**|**Default**|
| ----- | ------------- | ------------------- | ---------- | --------- |
|Default.OuputFile|The file where to redirect the standard output (stdout) of the test process|	Unix writeable (by the “vitraux” user) file in an existing directory|Mandatory|/usr/share/tomcat6/webapps/results/output|
|Default.OutputFile.Suffix|The suffix of the output file|Output file suffix|Mandatory|.txt|
|Default.ReportFile|The file to which to write the temporary test report||Mandatory|/usr/share/tomcat6/webapps/results/report.txt|
|JMeter.Executable|JMeter executable file|Filepath to JMeter executable|Only for Maestro|	/usr/local/tools/apache-jmeter-2.11/bin/jmeter.sh|
|JMeter.Jar|JMeter jar library|Filepath to JMeter jar library|Only for Maestro|/usr/local/tools/apache-jmeter-2.11/bin/ApacheJMeter.jar|
|WebPageTest.Executable|The executable for WebPageTest|Executable name or filepath|Only for Maestro|webpagetest|
|WebPageTest.Agent|The bash script for the WebPageTest agent|Executable name or filepath for the WPT agent|Only for Maestro|/usr/local/tools/webpagetest/agent/js/wptdriver.sh|
|WebPageTest.Timeout|The timeout of WebPageTest in seconds|Integer of timeout in seconds|	Only for Maestro|600|
|Monitoring.Username|The account to use for remote monitoring|User account to ssh to other hosts|Optional|vitraux|
|Monitoring.Output.Directory|The output directory for the monitoring results|Directory name|	Optional|./|
|Monitoring.Output.Suffix|The default suffix for monitoring results|File suffix|Optional|.txt|
|Monitoring.Sampling|The default sampling rate in seconds|Integer in seconds|Optional|1|
|Vmstat.Sampling|The sampling rate for vmstat based monitors in seconds|Integer in seconds|	Optional|1|
|Vmstat.MaxMemory|The maximum amount of memory in vmstat graphs in MB|Integer in MBs|Optional|	8192|

*Note:* The typical parameters that might require changes in most Vitraux deployments are the following:
- Default.OuputFile
- Default.ReportFile
- JMeter.Executable
- JMeter.Jar