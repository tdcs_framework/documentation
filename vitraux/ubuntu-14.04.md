# Set up a Linux server
A Vitraux server can act as a test automation server (Vitraux-Virtuoso), performance testing server (Vitraux-Maestro), or both (Vitraux). Vitraux is currently only supported for Linux as it makes use of external third-party tools for Linux.

## Hardware Specifications
The recommended specifications for these servers are:

**Vitraux-Virtuoso:**
- CentOS 6.5, CentOS 7, Ubuntu 14.04, Ubuntu 16.04
- Dual-Core 64-bit
- 4GB RAM
- 60GB disk space

**Vitraux-Maestro:**
- CentOS 6.5, CentOS 7, Ubuntu 14.04, Ubuntu 16.04
- Quad-Core 64-bit
- 8GB RAM
- 60GB disk space

The highest spec'd Vitraux-Maestro can be used, if Vitraux is to serve both types of tests. 

## Packages
The following packages are required for Vitraux. 

### Vitraux
- Java8 in either OpenJDK or OracleJDK
- Apache Tomcat
- MySQL server
- MySQL Connector/J (for CentOS 6.5 and Ubuntu 14.04)

### Virtuoso
- Apache Maven v3.0+
- Xvfb
- Firefox
- Chrome (optional)
- Selenium Chromedriver (if using Chrome)
- Vitraux-Virtuoso servlet war

### Maestro
- Apache JMeter
- System Stats
- Gnu plot
- Vitraux-Maestro servlet war

## Firewall rules
**To accept requests:** The port where the Tomcat server listens for requests needs to be opened. By default, this is TCP port 8080. Traffic to that port is expected from any Jenkins server that has a Vitraux-plugin installed or from a proxy server between such a Jenkins server and Vitraux.

**To run tests:** Access from the Vitraux server to the system under test (SUT) is required. The specific ports that have to be opened depend on the services that are tested on the SUT.

**To monitor servers:** Port 22, used for ssh, needs to be opened to all servers that a Vitraux-Maestro server will be monitoring, during a performance test run. Database server ports of the SUT might also need to be opened to traffic from Vitraux if these servers are also monitored.

**To resolve dependencies:** For Vitraux-Virtuoso 1.4 onwards, access to an artefact repository (e.g Nexus, Artifactory, etc.) that is used by test engineers and which contains releases of the Virtuoso library is recommended. If no such repository exists, then public Maven repositories can be used in combination with local deployments of Virtuoso library releases on the Vitraux server.

**To perform maintenance:** If possible, the Vitraux server should allow outgoing traffic towards the Internet to perform maintenance tasks and resolve dependencies using public repositories. Inbound traffic to port 22 should be allowed.

## Test Runner Account
A new user account, "vitraux” needs to be created for that server. This account:
- will be used to execute the Tomcat server, instead of the default “tomcat” one
- will need to have access to all directories and executables that Vitraux
- will need to have password-less public-key authentication set up to all servers that have to be monitored in a Vitraux-Maestro execution
- if local repositories are required, then `settings.xml` under the vitraux user `/home/vitraux/.m2/` should be updated

## Ubuntu 14.04 Script
This script will install Vitraux with Virtuoso and/or Maestro components. 
It has been tested on clean install of Ubunutu 14.04LTS Server and the installation requires access to the repositories and URLs listed in the script or amended to reference accessible versions.

```
#!/bin/sh

##########################################################################################
###################################### UBUNTU 14.04 ######################################
##########################################################################################

# This has been tested on a default install of Ubuntu 14.04LTS Server

# The following packages are required to install
# Where possible the default ubuntu repositories are used
# Otherwise additional repos or files are downloaded

# Vitraux-Core
# - openjdk-8-jdk-headless (adds repo - ppa:openjdk-r/ppa)
# - tomcat7
# - mysql-server
# - mysql-connector (download - http://dev.mysql.com/get/Downloads/Connector-J/)
# - zip
# - unzip

# Vitraux-Virtuoso
# - Maven (download)
# - Xvfb
# - Firefox
# - Chrome [optional] (adds repo - http://dl.google.com/linux/chrome/deb/ )
# - Chromedriver [optional] (download - http://chromedriver.storage.googleapis.com/)
# - Vitraux-Virtuoso.war (download - https://gitlab.com/tdcs_framework/maven/raw/master/repository/uk/gov/homeoffice/vitraux/)

# Vitraux-Maestro
# - sysstat
# - gnuplot
# - jmeter (download - http://mirror.vorboss.net/apache/maven/maven-3/)
# - Vitraux-maestro.war (download - https://gitlab.com/tdcs_framework/maven/raw/master/repository/uk/gov/homeoffice/vitraux/)

# First up, check the script is run with sudo
if [ "$(whoami)" != 'root' ]; then
    echo $"You must have elevated privileges to run this script. Execute it again using sudo."
    exit 1;
fi

##########################################################################################
####################################### PARAMETERS #######################################
##########################################################################################

# Not yet used!

# Vitraux Core
tomcat_version="7"
mysql_connector_version="5.1.39"
mysql_connector_dir="mysql-connector-java-"$mysql_connector_version"
mysql_connector_file=$mysql_connector_dir".tar.gz"
mysql_connector_url="http://dev.mysql.com/get/Downloads/Connector-J/"$mysql_connector_file

vitraux_url="https://gitlab.com/tdcs_framework/maven/raw/master/repository/uk/gov/homeoffice/vitraux/"

# Vitraux-Virtuoso
maven_version="3.3.9"
maven_dir="apache-maven-"$maven_version"
maven_file=$maven_dir"-bin.tar.gz"
maven_url="http://mirror.vorboss.net/apache/maven/maven-3/"maven_version"/binaries/"$maven_file

chromedriver_version="2.21"
chromedriver_url="http://chromedriver.storage.googleapis.com/"$chromedriver_version"/chromedriver_linux64.zip"

vitraux_virtuoso_version="1.4.6"
vitraux_virtuoso_war="virtuoso-"$vitraux_virtuoso_version".war"
vitraux_virtuoso_url=$vitraux_url"virtuoso/"$vitraux_virtuoso_version"/"$vitraux_virtuoso_war

# Vitraux-Maestro
jmeter_version="2.13"
jmeter_dir="apache-jmeter-"$jmeter_version
jmeter_file=$jmeter_dir".tgz"
jmeter_url="http://mirror.vorboss.net/apache//jmeter/binaries/"$jmeter_file

vitraux_maestro_version="1.4.6"
vitraux_maestro_war="virtuoso-"$vitraux_maestro_version".war"
vitraux_maestro_url=$vitraux_url"maestro/"$vitraux_maestro_version"/"$vitraux_maestro_war

##########################################################################################
###################################### VITRAUX CORE ######################################
##########################################################################################

cat > vitraux-core.sh <<EOL
#!/bin/sh

# Configure MySQL root password for non-interactive install
echo "mysql-server mysql-server/root_password password admin" | debconf-set-selections
echo "mysql-server mysql-server/root_password_again password admin" | debconf-set-selections

# Add openjdk-8 repository
add-apt-repository ppa:openjdk-r/ppa -y
apt-get update

# Install packages
# - zip
# - unzip
# - openjdk-8-jdk-headless
# - tomcat7
# - mysql-server
apt-get install zip unzip openjdk-8-jdk-headless tomcat7 mysql-server -y

# Set java version to java8 and update variables
echo -e "/usr/lib/jvm/java-8-openjdk-amd64/jre/bin/java" | update-alternatives --config java
sed -r -i 's@:/usr/local/games"@:/usr/local/games:$JAVA_HOME/bin:$JRE_HOME/bin"@g' /etc/environment
echo 'JAVA_HOME="/usr/lib/jvm/java-8-openjdk-amd64"' >> /etc/environment
echo 'JRE_HOME="/usr/lib/jvm/java-8-oracle/jre"' >> /etc/environment

# Add the Vitraux user who will run tomcat and maven
# maven will use the local repository /home/vitraux/.m2/repository
useradd -m vitraux

# Configure tomcat to run as the vitraux user
service tomcat7 stop

usermod -a -G tomcat7 vitraux
sed -r -i 's/TOMCAT7_USER=tomcat7/TOMCAT7_USER=vitraux/g' /etc/default/tomcat7
sed -r -i 's/TOMCAT7_GROUP=tomcat7/TOMCAT7_GROUP=vitraux/g' /etc/default/tomcat7
chown -R vitraux.vitraux /var/cache/tomcat7/
chown -R vitraux.vitraux /var/lib/tomcat7/webapps/
chown -R vitraux.vitraux /var/log/tomcat7/
chown -R vitraux.vitraux /etc/tomcat7/
chown -R vitraux.vitraux /etc/logrotate.d/tomcat7

# Amend tomcat7 to run with java8
sed -r -i 's@#JAVA_HOME=/usr/lib/jvm/openjdk-6-jdk@JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64@g' /etc/default/tomcat7

# Results from the test will be zipped and Jenkins will then take collect the results from localhost:8080/results/{Name of Test}.zip
mkdir /var/lib/tomcat7/webapps/results && sudo chown -R vitraux.vitraux /var/lib/tomcat7/webapps/results

# Add mysql-connector 
wget http://dev.mysql.com/get/Downloads/Connector-J/mysql-connector-java-5.1.39.tar.gz
tar xvf mysql-connector-java-5.1.39.tar.gz mysql-connector-java-5.1.39/mysql-connector-java-5.1.39-bin.jar
mv mysql-connector-java-5.1.39/mysql-connector-java-5.1.39-bin.jar /usr/share/tomcat7/lib
rm -rf mysql-connector-java-5.1.39
rm mysql-connector-java-5.1.39.tar.gz

# Restart the service to run as vitraux user
service tomcat7 start

# Create the Vitraux database
mysql --user=root --password=admin < vitraux-database.sql
EOL

# Create the vitraux database structure
# Also available for vitraux source in src/main/resource/db/database.sql
cat > vitraux-database.sql <<EOL
CREATE DATABASE IF NOT EXISTS vitraux;
USE vitraux;

CREATE TABLE IF NOT EXISTS ci_environment (
	ip varchar(32) NOT NULL,	
	type varchar(256) NOT NULL,
	info text,
	PRIMARY KEY (ip)
);

CREATE TABLE IF NOT EXISTS perftest_suite (
	id int NOT NULL AUTO_INCREMENT,
	installation_dir varchar(256) NOT NULL,
	test_name varchar(256) NOT NULL,
	project_name varchar(256) NOT NULL,
	PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS perftest_suite_execution (
	id int NOT NULL AUTO_INCREMENT,
	build int NOT NULL,
	perftest_suite_id int NOT NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (perftest_suite_id) REFERENCES perftest_suite(id)
);	

CREATE TABLE IF NOT EXISTS perftest_suite_execution_results (
	id int NOT NULL AUTO_INCREMENT,
	type varchar(256) NOT NULL,
	value varchar(256) NOT NULL,
	perftest_suite_execution_id int NOT NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (perftest_suite_execution_id) REFERENCES perftest_suite_execution(id)		
);
EOL

##########################################################################################
#################################### VITRAUX VIRTUOSO ####################################
##########################################################################################

cat > vitraux-virtuoso.sh <<EOL
#!/bin/sh

# Install 
# - Maven - build tool
# - Xvfb - virtual frame buffer to run webbrowers headless
# - Firefox
# - Google Chrome

# Maven
wget http://mirror.vorboss.net/apache/maven/maven-3/3.3.9/binaries/apache-maven-3.3.9-bin.tar.gz
tar -zxf apache-maven-3.3.9-bin.tar.gz
mv apache-maven-3.3.9 /usr/local
ln -s /usr/local/apache-maven-3.3.9/bin/mvn /usr/bin/mvn
rm apache-maven-3.3.9-bin.tar.gz

apt-get install xvfb firefox -y

# Add Virtuoso folders
# /var/data/virtuoso is used to store virtuoso jobs
# /var/data/log/virtuoso is used to log virtuoso execution
mkdir -p /var/data/virtuoso && sudo chown -R vitraux.vitraux /var/data/virtuoso
mkdir -p /var/data/log/virtuoso && sudo chown -R vitraux.vitraux /var/data/log/virtuoso

# Add Xvfb as service

cat > /etc/init.d/xvfb <<EOLxvfb
#! /bin/sh

### BEGIN INIT INFO
# Provides: Xvfb
# Required-Start: $local_fs $remote_fs
# Required-Stop:
# X-Start-Before:
# Default-Start: 2 3 4 5
# Default-Stop: 0 1 6
# Short-Description: Loads X Virtual Frame Buffer
### END INIT INFO

XVFB=/usr/bin/Xvfb
XVFBARGS=":99 -screen 0 1024x768x8 -ac"
PIDFILE=/var/run/xvfb.pid
case "\$1" in
  start)
    echo -n "Starting virtual X frame buffer: Xvfb"
    start-stop-daemon --start --quiet --pidfile $PIDFILE --make-pidfile --background --exec $XVFB -- $XVFBARGS
    echo "."
    ;;
  stop)
    echo -n "Stopping virtual X frame buffer: Xvfb"
    start-stop-daemon --stop --quiet --pidfile $PIDFILE
    echo "."
    ;;
  restart)
    $0 stop
    $0 start
    ;;
  status)
    echo -n "Status virtual X frame buffer: Xvfb"
    echo
    ps -ef | grep Xvfb
    echo "."
    ;;
  *)
        echo "Usage: /etc/init.d/xvfb {start|stop|restart|status}"
        exit 1
esac

exit 0
EOLxvfb

# cat for /etc/init.d/xvfb loses $1 so amend
sed -r -i 's@case "" in@case "$1" in@g' /etc/init.d/xvfb

# Set service to start on boot
chmod +x /etc/init.d/xvfb
update-rc.d xvfb defaults
service xvfb start

# Set DISPLAY for this session
export DISPLAY=:99
# Set DISPLAY variable on boot
echo 'DISPLAY=:99' >> /etc/environment
EOL

cat > vitraux-chrome.sh <<EOL

# Add Chrome repository
wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | sudo apt-key add - 
sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list'
apt-get update

# Install
apt-get google-chrome-stable -y

# Chromedriver to use Chrome with Selenium
wget http://chromedriver.storage.googleapis.com/2.21/chromedriver_linux64.zip
unzip chromedriver_linux64.zip
chmod +x chromedriver
mv -f chromedriver /usr/local/share/chromedriver
ln -s /usr/local/share/chromedriver /usr/local/bin/chromedriver
ln -s /usr/local/share/chromedriver /usr/bin/chromedriver
rm chromedriver_linux64.zip
EOL

# Deploy virtuoso.war
cat > vitraux-virtuoso-war.sh <<EOL
echo Fetching vitraux-virtuoso from gitlab repository
wget https://gitlab.com/tdcs_framework/maven/raw/master/repository/uk/gov/homeoffice/vitraux/virtuoso/1.4.6/virtuoso-1.4.6.war
echo Deploying to /var/lib/tomcat7/webapps/
mv -f virtuoso-1.4.6.war /var/lib/tomcat7/webapps/virtuoso.war

echo Waiting for virtuoso.war to deploy
until [ "\`curl -s -o /dev/null -I -w '%{http_code}' localhost:8080/virtuoso/VirtuosoServer?type=status\`" = "200" ];
do
  echo --- sleeping for 5 seconds
  sleep 5
done

echo Amending configuration files
sed -r -i 's@maxIdle="3" />@maxIdle="3" \n        factory="org.apache.commons.dbcp.BasicDataSourceFactory" />@g' /var/lib/tomcat7/webapps/virtuoso/META-INF/context.xml
echo
EOL

##########################################################################################
#################################### VITRAUX MAESTRO #####################################
##########################################################################################

cat > vitraux-maestro.sh <<EOL
#!/bin/sh

apt-get install sysstat gnuplot -y

# Add Maestro folders
# /var/data/virtuoso is used to store virtuoso jobs
# /var/data/log/virtuoso is used to log virtuoso execution
mkdir -p /var/data/maestro && sudo chown -R vitraux.vitraux /var/data/maestro
mkdir -p /var/data/log/maestro && sudo chown -R vitraux.vitraux /var/data/log/maestro

# Install Jmeter2.13
# apt-get jmeter will only install 2.11.20151206
wget http://mirror.vorboss.net/apache//jmeter/binaries/apache-jmeter-2.13.tgz
tar xvf apache-jmeter-2.13.tgz
mkdir /usr/local/tools
mv apache-jmeter-2.13 /usr/local/tools/apache-jmeter-2.13
rm apache-jmeter-2.13.tgz

# SysStat config
sed -r -i 's/ENABLED="false"/ENABLED="true"/g' /etc/default/sysstat
sed -r -i 's@5-55/10@*/1@g' /etc/cron.d/sysstat
sudo service sysstat restart
EOL

cat > vitraux-maestro-war.sh <<EOL
# Deploy maestro.war
echo Fetching vitraux-maestro from gitlab repository
wget https://gitlab.com/tdcs_framework/maven/raw/master/repository/uk/gov/homeoffice/vitraux/maestro/1.4.6/maestro-1.4.6.war
echo Deploying to /var/lib/tomcat7/webapps
mv -f maestro-1.4.6.war /var/lib/tomcat7/webapps/maestro.war

echo Waiting for maestro.war to deploy
until [ "\`curl -s -o /dev/null -I -w '%{http_code}' localhost:8080/maestro/MaestroServer?type=status\`" = "200" ];
do
  echo --- sleeping for 5 seconds
  sleep 5
done

echo Amending configuration files
sed -r -i 's@maxIdle="3" />@maxIdle="3" \n        factory="org.apache.commons.dbcp.BasicDataSourceFactory" />@g' /var/lib/tomcat7/webapps/maestro/META-INF/context.xml
sed -r -i 's@/usr/share/tomcat6/webapps/results/output@/var/lib/tomcat7/webapps/results/output@g' /var/lib/tomcat7/webapps/maestro/WEB-INF/classes/config.properties
sed -r -i 's@/usr/share/tomcat6/webapps/results/report.txt@/var/lib/tomcat7/webapps/results/report.txt@g' /var/lib/tomcat7/webapps/maestro/WEB-INF/classes/config.properties
sed -r -i 's@/usr/local/tools/apache-jmeter-2.11/@/usr/local/tools/apache-jmeter-2.13/@g' /var/lib/tomcat7/webapps/maestro/WEB-INF/classes/config.properties
echo
EOL

##########################################################################################
###################################### VITRAUX WAR #######################################
##########################################################################################

#Remove virtuoso and maestro war

cat > vitraux-remove-war.sh <<EOL
#!/bin/sh

service tomcat7 stop

rm -rf /var/lib/tomcat7/webapps/virtuoso
rm /var/lib/tomcat7/webapps/virtuoso.war

rm -rf /var/lib/tomcat7/webapps/maestro
rm /var/lib/tomcat7/webapps/maestro.war

service tomcat7 start

echo "tomcat7 restarted vitraux-virtuoso/vitraux-maestro removed"
EOL

##########################################################################################
######################################## INSTALL #########################################
##########################################################################################

chmod +x vitraux-core.sh
chmod +x vitraux-virtuoso.sh
chmod +x vitraux-chrome.sh
chmod +x vitraux-virtuoso-war.sh 
chmod +x vitraux-maestro.sh
chmod +x vitraux-maestro-war.sh 
chmod +x vitraux-remove-war.sh

echo "What Vitraux components to you wish to install?"
options=("Virtuoso" "Virtuoso with Chrome" "Maestro" "Both")
select VitrauxComponent in "${options[@]}" "Quit"; do
	case $VitrauxComponent in
		"Virtuoso" )
			echo "Install Vitraux-Virtuoso"
			. vitraux-core.sh
			. vitraux-virtuoso.sh
			. vitraux-virtuoso-war.sh
			echo "Components installed"
			break
			;;
		"Virtuoso with Chrome" )
			echo "Installing Vitraux-Virtuoso with Chrome"
			. vitraux-core.sh
			. vitraux-virtuoso.sh
			. vitraux-chrome.sh
			. vitraux-virtuoso-war.sh
			echo "Components installed"
			break
			;;			
		"Maestro" )
			echo "Installing Vitraux-Maestro"
			. vitraux-core.sh
			. vitraux-maestro.sh
			. vitraux-maestro-war.sh
			echo "Components installed"
			break
			;;
		"Both" )
			echo "Installing Vitraux-Virtuoso and Vitraux-Maestro"
			. vitraux-core.sh
			. vitraux-virtuoso.sh
			. vitraux-chrome.sh
			. vitraux-maestro.sh
			. vitraux-virtuoso-war.sh
			. vitraux-maestro-war.sh
			echo "Components installed"
			break
			;;
		"Quit" )
			break
			;;
		*) echo invalid option;;
	esac
done


if [ $VitrauxComponent != "Quit" ]
then

	# Restarting tomcat
	echo Restarting tomcat
	service tomcat7 restart
	sleep 10    

	case $VitrauxComponent in
		"Virtuoso" )
			echo Checking Vitraux-Virtuoso Status
			curl localhost:8080/virtuoso/VirtuosoServer?type=status
			;;
		"Virtuoso with Chrome" )
			echo Checking Vitraux-Virtuoso Status
			curl localhost:8080/virtuoso/VirtuosoServer?type=status
			;;
		"Maestro" )
			echo Checking Vitraux-Maestro Status
			curl localhost:8080/maestro/MaestroServer?type=status
			;;
		"Both" )
			echo Checking Vitraux-Virtuoso Status
			curl localhost:8080/virtuoso/VirtuosoServer?type=status
			echo Checking Vitraux-Maestro Status
			curl localhost:8080/maestro/MaestroServer?type=status
			;;
	esac

fi

##########################################################################################
######################################## FINISHED ########################################
##########################################################################################

```