Virtuoso is a Java library that is used to facilitate the development and maintenance of test automation suites. This is achieved in three ways:

1. Using classes to abstract common functionalities (capabilities) that are encountered in a wide range of systems under test (SUTs), e.g. UI-driven actions, database interactions, web service requests etc.
2. By using a common approach for test data input and test reporting.
3. By promoting a structured way to develop tests, which separates test logic from system-specific implementation. 

Virtuoso currently uses JUnit as its test execution engine. Test engineers that want to use Virtuoso will need to include it as a dependency to their test automation suites and extend their test classes from Virtuoso base classes (see [Requirements](requirements)). 

One of the main design principles of Virtuoso is to offer its capabilities in a transparent way. No explicit initialisation or finalization actions are required in the test classes, while a set of naming conventions are followed to automatically link input data and reports to tests. Test executions can be parameterised with a key-value fashion (see [Configuration](configuration)) to better address the needs of different automation tests. 

The following image shows the high-level architecture of the Virtuoso library. 

![Virtuoso Logical Architecture](virtuoso/images/virtuosologicalarchitecture.png)

At the top is the “Test” node, where the steps of the test script are coded and the “Test Data” that define input values and expected results for the test steps. The test steps can also be generated via Cucumber-JVM using Gherkin-syntax based feature files. Data can also be defined in feature files as “Examples”. This level is also accessible by Test Analysts, who can update test data files and feature files, as well as review the test classes, which can be designed in such a way that they hide implementation details and are appropriate to be read by non-technical resources.

The test class invokes the methods of “Target Objects” which implement the specific link to the system under test. These are developed, read, and maintained by test engineers. The interfaces to the “Target Objects” should be clear enough, so that the test classes that invoke them are easily understood.

Configuration data can be used to parameterise a test execution for various environments and contexts.

Blue-coloured nodes relate to the core framework code and need not be processed during the development of a test automation suite. They are used to offer the capabilities, test data management and reporting features of the Virtuoso framework in a transparent way.
