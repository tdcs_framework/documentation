The current version of the Vitraux Server is 1.13. The supported capabilities are:

**Vitraux-Plugin 1.13**
- Support for multiple environment files sent as a list of comma-separated (without spaces) files
- Support for separate Test Data to be executed with the same test suite
- Jenkins user-defined parameters can be used as key-values in Virtuoso and Maestro tests
- Support for the Virtuoso Server 1.4 and Maestro Server 1.4
- Terminate running test

**Vitraux-Plugin 1.12**
- Communication with Vitraux server to trigger automation and performance tests
- Environment properties support
- Proxy support
- Timeouts

