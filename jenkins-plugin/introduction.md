The Vitraux plugin is a Jenkins plugin that is used to interact with a Vitraux server from a Jenkins instance. Vitraux servers provide test automation and performance testing execution services.

The responsibility of the plugin is the communication with such servers and entails the following:
- To send the zip with the test suite to be executed to the Vitraux server.
- To monitor the execution as it is happening on the console output of the Jenkins build.
- To provide reports on the test results.
- To terminate a test upon request.

These actions are provided by the API of a Vitraux server and thus the main role of the plugin is to facilitate the request formation for this API, based on the configuration parameters of a Jenkins job.

Using the plugin also allows the integration of a test automation or performance testing suite in the workflow of a Jenkins build, triggered periodically or at the back of some specific action, etc.

The workflow of a typical configuration of a Vitraux job is as follows
1. Checkout the test suite from a repository – either the head or a previous revision.
2. Build the test suite and wrap it up in an appropriate format for Vitraux automation or performance test execution.
3. Send it over to Vitraux for execution.
4. Wait for the execution to finish (or an optional timeout to expire).
5. Copy over the results of the execution from the Vitraux server to the Jenkins instance disk.
6. Generate the report for the build.

