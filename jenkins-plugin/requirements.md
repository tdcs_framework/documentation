To function correctly the Vitraux plugin has the following requirements.

- Jenkins instance with version higher than 1.504.
- The directory were the plugin is extracted should be `plugins/apt-plugin`. This is the default extraction directory, when the plugin is installed. It should not be manually changed.
- The parent directory of the `plugins` should include a `jobs` directory. This is the default file structure of a Jenkins instance.
- The user account that runs Jenkins and executes the Jenkins jobs needs to have write permissions to the `plugins/apt-plugin` and the `plugins/../jobs/` directory.

The last two requirements are necessary, because results are copied over from the Vitraux server to the Jenkins instance filesystem on a per-build basis. Result files also need to be accessed via a URI and thus a symbolic link is created by the plugin so that the `plugin/apt-plugin/results/` location points to the root of the results.

